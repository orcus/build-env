# build-env

Build environment for orcus which includes all its dependencies.

## Building

### Windows via CMake

All the instructions here are given with the assumption that you are using MINGW
as your shell environment.  If you use a different shell environment, make adjustment
accordingly.

#### Build Boost

Download the latest source package from https://boost.org, unpack it, and
change directory into it.  Once you are in, run the following command:

```bash
./bootstrap.bat
./b2 --stagedir=./stage/x64 address-model=64 link=static --build-type=complete -j $(nproc)
```

and wait until it's done.  This will build Boost as static libraries.

#### Build orcus

Clone the git repo, cd into it, and ensure that all the submodules are also
checked out.

Once done, run the following commands:

```bash
cd cmake
mkdir build
cd build
cmake .. \
    -DCMAKE_BUILD_TYPE=Release \
    -DBOOST_INCLUDEDIR=/path/to/boost \
    -DBOOST_LIBRARYDIR=/path/to/boost/stage/x64/lib \
    -DBoost_USE_STATIC_LIBS=1
cmake --build . --config Release
```

and wait until it finishes.  Both ixion and orcus will have been installed
in `$PWD/install`.  If you need to install them to a different location,
specify it via `CMAKE_INSTALL_PREFIX`.

#### Build orcus with Parquet enabled

You can optionally build orcus with support for the Apache Parquet format by
specifying `-DORCUS_WITH_PARQUET=ON` in your cmake configuration.  But do note
that when setting this option, it will build the Apache Arrow library which will
significantly add to your build time.
