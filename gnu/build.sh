#!/usr/bin/env bash

set -e

INSTALLDIR=$PWD/install
export PKG_CONFIG_PATH=$INSTALLDIR/share/pkgconfig:$INSTALLDIR/lib/pkgconfig
export LD_LIBRARY_PATH=$INSTALLDIR/lib64:$INSTALLDIR/lib
export CPPFLAGS="-march=native $CPPFLAGS"

if [ -d $INSTALLDIR/include/mdds-2.0 ]; then
    echo "mdds is already installed."
else
    pushd ../mdds
    ./autogen.sh --prefix=$INSTALLDIR
    make -j$(nproc) install
    popd
fi

if [ -d $INSTALLDIR/include/libixion-0.17 ]; then
    echo "ixion is already installed."
else
    pushd ../ixion
    ./autogen.sh --prefix=$INSTALLDIR
    make -j$(nproc) install
    popd
fi

if [ -d $INSTALLDIR/include/liborcus-0.17 ]; then
    echo "orcus is already installed."
else
    pushd ../orcus
    ./autogen.sh --prefix=$INSTALLDIR --with-cpu-features
    make -j$(nproc) install
    popd
fi
